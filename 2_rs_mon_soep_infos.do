/*
**********************************************************************************************************************
** 0: Zusammenf�gen der Infos f�r Deskription	 				                                                              				**
**********************************************************************************************************************
use "$path_temp\stat1.dta", clear				// Allgemein letzte EWT-Stati
gen dur = rsbeg - beginy

rename (persnr)(pid) 
local keepvars p0622 pmonin iyear p0354 p0356 p0488 p4132 p4133
merge 1:m pid using "$soep_in\pl.dta", keepus(`keepvars')
keep if _m == 3
drop _m

local keepvars pid svyyear welle p0622 pmonin iyear p0354 p0356 p0488 p4132 p4133
use `keepvars' using "$soep_in\pl.dta", clear				// Allgemein letzte EWT-Stati

rename (pid) (persnr)
merge m:1 persnr using "$path_temp\stat1.dta"
keep if _m == 3

rename p0622 lz

gen d_rs = svyyear - rsbeg
keep if inrange(d_rs,-6,8)

keep persnr d_rs lz spelltyp rsbeg dur
// der folgende Abbruch scheint ein bug in stata zu sein:
rename lz lz_

recode spelltyp (1 2 3=1) (4=2) (5=3) (6=4) (7=6) (else=7)
replace spelltyp = 5 if spelltyp == 4 & inrange(dur,0,5)

lab def typ	1 "1 education" 2 "2 ft-empl." 3 "3 pt-empl." 4 "4 long-term unempl." 5 "5 short-term unempl." ///
			6 "6 housemaker" 7 "7 empl. in ret." 8 "8 else" , replace
lab val spelltyp typ

sort persnr d_rs 


replace d_rs = d_rs + 6
anova lz d_rs 
margins d_rs
set scheme lean2
global gropts1 ///
	plot1opts(lcolor(gs9) mcolor(gs9) lwidth(thick)) ci1opts(lcolor(gs9))						///
	legend(off) 
marginsplot, ylabel(5 (1) 8) $gropts1 xline(6)

anova lz d_rs##spelltyp
qui margins d_rs#spelltyp, vsquish
marginsplot, ylabel(5 (1) 8) xline(6)

anova lz persnr d_rs##spelltyp


tostring d_rs, replace
foreach num of numlist 6 / 1 {
	replace d_rs = "n`num'" if d_rs == "-`num'"  
	}
tab d_rs

***! RESHAPE und SORTIEREN
reshape wide lz_, i(persnr) j(d_rs) string

*/


**********************************************************************************************************************
** 1: Zusammenf�gen der Infos	 				                                                              				**
**********************************************************************************************************************
local keepvars pid svyyear sex gebjahr gebmonat netto iyear psample loc1989
use `keepvars' using "$soep_in\ppfadl.dta", clear
numlabel _all, add
*tab netto
*keep if inlist(netto,10,12,13,14,15,19,31)					// Zeitpunkte gesamt


local keepvars pid svyyear pgerwtyp pgemplst pgmonth pgsbil pgbilzt pgisced pgfamstd
	*		pglabnet
merge m:1 pid svyyear using "$soep_in\pgen.dta", keepus(`keepvars')		// Gen-Personen-File

drop _m
rename pgsbil  bild4
rename pgbilzt bildzeit4
rename pgisced isced4
rename pgmonth intmon4
rename pgfamstd famstd

* mvdecode pglabnet, mv(-1=.a \ -2=.b \ -3 = .c)
* gen alter = svyyear - gebjahr
* tabstat pglabnet, s(mean sd n) by(alter)
* rename pglabnet ek

* pgexpft pgexppt pgexpue --> Arbeitsmarkterfahrung!
keep pid sex gebjahr loc1989 psample gebmonat netto iyear svyyear bild4 bildzeit4 isced4 intmon4 famstd

local keepvars i11102 
merge 1:1 pid svyyear using "$soep_in\pequiv.dta", keepus(`keepvars')	// Nettoequivalenz-Personen-File
tab _m
drop if _m == 1
drop _m

rename i11102 hhek			// HH Post-Government Income

rename (pid) (persnr)
merge m:1 persnr using "$path_temp\biospell_1.dta"				// BIOSPELL-Daten
rename (persnr)(pid) 
drop _m

tab rsj if pid != pid[_n-1], m
drop if rsj == .
d, s															// 133,253 Beobachtungen

count if pid != pid[_n+1]										// f�r die 10140 Personen
saveold "$path_temp\aa_rs.dta", replace

**********************************************************************************************************************
** 4: Lebenszufriedenheit ranspielen und Variablen generieren			 				                                                              				**
**********************************************************************************************************************

local keepvars p0622 pmonin iyear p0354 p0356 p0488 p4132 p4133

		
merge 1:1 pid svyyear using "$soep_in\pl.dta", keepus(`keepvars')
keep if _m == 3
drop _m

rename p0622 lz
rename iyear intjahr
rename pmonin intmon
rename p0354 gr_ren1
rename p0356 gr_ren2
rename p0488 sG

mvdecode p4132 p4133, mv(-2 -1 = .a)
egen fG = rowmean(p4132 p4133) 

drop intmon4 p4132 p4133

count if lz == .


* tabstat gebjahr gebmonat bild4 pstyp aaj gr_ren1 loc1989 netto bildzeit4 rsj ///
*		aam gr_ren2 sex psample intjahr isced4 rsm intmon lz, s(n) by(svyyear) // perfetto!


numlabel _all, add force
mvdecode _all, mv(-1=.a \ -2=.b \ -3 = .c)
count if pid == pid[_n+1] & aaj != aaj[_n+1]	//ok: keine Wechsel des YL innerhalb einer Person
count if pid == pid[_n+1] & rsj != rsj[_n+1]	//ok: keine Wechsel des YL innerhalb einer Person

d, s															// 104,430 Beobachten (4130 weniger) Beobachtungen

count if pid != pid[_n+1]										// f�r die 8727 Personen
saveold "$path_temp\aa_rs2.dta", replace

// fr�her: 9758 Personen sollten es sein 

use "$path_temp\aa_rs2.dta", clear
rename pid fallnum
rename svyyear welle

sort fallnum intjahr
count if fallnum != fallnum[_n+1]				// 8727 Pers

tab job if fallnum != fallnum[_n+1]

gen d_aa = intjahr - aaj
lab var d_aa "Distanz Austritt Arb. zu Befragungszeitpunkt" 
*tab d_aa
/* sieht gut aus! */

gen a_aa = aaj - (gebjahr)
lab var a_aa "Alter zum Arbeitsausstiegs"
tab a_aa, m
*replace a_aa = .a if a_aa == .			// keine aa bekannt, da kein vorheriger Spell vorhanden
/* sehr gut */
count if a_aa != a_aa[_n+1] & fallnum == fallnum[_n+1]	//ok

gen d_rs = intjahr - rsj
lab var d_rs "Distanz zum Ruhestandsjahr" 
tab d_rs
/* sieht gut aus! */

gen a_rs = rsj - (gebjahr)
lab var a_rs "Alter zum Ruhestandsjahr"
tab a_rs, m
/* sehr gut */

count															// 104430 Beobachtungen
count if fallnum != fallnum[_n+1]								//  8727  Personen 

lab var d_bet "Distanz zwischen Austritt und Eintritt"

gen art = -1
lab var art "Weg des �bergangs: 1 = direkt, 2 = indirekt"

replace art = 1 if d_bet == 0 									//(innerhalb des selben jahres)
replace art = 2 if d_bet >=  1

list fallnum aaj rsj d_bet art in 1/25

for any 1 2 3: tab d_b art if fallnum != fallnum[_n+1] & job == X

sort fallnum welle
tab art 
tab art if fallnum != fallnum[_n+1]
tab2 job sex if fallnum != fallnum[_n+1], col
tab2 d_bet job if fallnum != fallnum[_n+1]

gen drop = .a
replace drop = 1 if job == 1 & d_bet > 1
replace drop = 1 if job == 2 & d_bet > 1
tab1 drop if fallnum != fallnum[_n+1]							//  739 Personen
drop if drop == 1												// 9264 Beobachtungen
drop drop
tab2 d_bet job if fallnum != fallnum[_n+1]

list fallnum d_aa d_rs if job == 1 in 1/50 ,clean
list fallnum d_aa d_rs if job == 2 in 1/400, clean
list fallnum d_aa d_rs if job == 3 in 1/100, clean

/*
Z�hle 2009,591:  geklumpte Lebensverl�ufe

				Gesamt 			Frauen 				M�nner
				Alle West Ost 	Alle 	West Ost   Alle  West Ost
Normalarbeitsverh�ltnis 	12,1 13,5 (7,0) (2,6) 	(2,5) (3,2)  20,4 23,5  (9,8)
Fr�hrentner aus Normalarbeit 	13,6 13,2 15,0 	11,6 	(10,8)(14,9) 15,3 15,4 (15,1)
Nichterwerbst�tige 		30,8 32,2 25,6 	39,7 	43,1  25,8   23,0 22,3 (25,5)
Renteneinsteiger aus Arbeitslo	26,5 23,0 39,2 	21,3 	16,8  39,7   31,1 28,7  38,9
Renteneinsteiger aus Teilzeit 	12,7 13,5 (9,7) 20,5 	21,7 (15,2) (5,8) (5,9) (5,5)
Selbstst�ndige 			4,4 4,6 (3,5) 	(4,3) 	(5,1) (1,2) (4,4) (4,2) (5,2)

Fallzahlen unter 30 in Klammern
Quelle: SOEP.

Die folgenden Auswertungen beruhen auf den Daten des Sozio-oekonomischen Panels(SOEP) 
(Wagner et al. 2008).Mit dieser L�ngsschnitterhebung ist es m�glich, aus einer 
Verlaufsperspektive vollst�ndige Erwerbsbiografien beim �bergang in den Ruhestand 
von ca. 800 Personen repr�sentativ abzubilden. Der Untersuchungszeitraum
umfasst die Jahre 1992 bis 2006, entsprechend die Geburtsjahrg�nge von 1937
bis 1941. Die Angaben beziehen sich auf Personen im Alter von 55 bis 65 Jahren, die
in Privathaushalten leben und f�r die im SOEP zu allen elf Erhebungsjahren vollst�ndige
Erwerbsinformationen vorliegen(balanced panel design).
*/



if $an == 1 {
/* 
histogram d_bet if fallnum != fallnum[_n+1], freq name(g1hist1, replace) nodraw
histogram d_bet2 if fallnum != fallnum[_n+1], freq name(g1hist2, replace) nodraw
histogram d_bet_a if fallnum != fallnum[_n+1], freq name(g1hist3, replace) nodraw
graph combine g1hist1 g1hist2 g1hist3
graph export 	"$path_log\g1_histo2_d_bet.png", replace
*/
histogram d_rs , frequ width(1) name(histo1, replace) nodraw
histogram d_aa , frequ width(1) name(histo2, replace) nodraw
histogram a_rs if fallnum != fallnum[_n+1], frequ width(1) name(histo3, replace) nodraw
histogram a_aa if fallnum != fallnum[_n+1], frequ width(1) name(histo4, replace) nodraw
graph combine histo1 histo2
graph export 	"$path_log\g_histo2_d_soep.png", replace
graph combine histo3 histo4
graph export 	"$path_log\g_histo2_a_soep.png", replace
histogram d_bet if fallnum != fallnum[_n+1], freq width(1) name(histo5, replace) 
graph export 	"$path_log\g_histo2_d_bet_soep.png", replace
}


//*************************
// Personen im AltersRange
//*************************
sort fallnum
gen a_rscheck = 1 if inrange(a_rs,57,65)			
tab a_rscheck, m									// von 83077 Beobachtunge
tab a_rscheck if fallnum != fallnum[_n+1], m		// 21353 Beobachtungen l�schen (20,5 %) !
tab2 a_rscheck sex if fallnum != fallnum[_n+1], m col		// fast ausgeglichen, st�rker Frauen (2 %) !
*tab2 gebjahr a_check, m	 						//  6,874 von 1,853 Personen l�schen (21 %)

*bro fallnum intjahr gebjahr* rsj a_rs if a_check == .

gen a_aacheck = 1 if inrange(a_aa,57,65)

tab a_aacheck, m									// von 61,849 Beobachtunge
tab a_aacheck if fallnum != fallnum[_n+1], m		// 42,581 Beobachtungen l�schen (0 %) !

tab2 a_rscheck a_aacheck, m
tab2 a_rscheck a_aacheck if fallnum != fallnum[_n+1], m	// 61,849 Beobachtungen / 5,158 Personen
gen n1 = 1 if fallnum != fallnum[_n+1]
bys job: tab2 a_rscheck a_aacheck if n1 == 1, m		// 3542 VZ, 702 TZ, 912 HH 
sort fallnum

drop if a_rscheck != 1

tab a_aacheck, m									// von 58,141 Beobachtunge
tab a_aacheck if fallnum != fallnum[_n+1], m		// 108 Beobachtungen l�schen (0 %) !
*tab2 gebjahr a_aacheck , m							//  7  von 3,659 Personen	    l�schen (0 %)


keep if a_rscheck == 1 & a_aacheck == 1					// Selektion: nur F�lle, die sowohl 
		// Ruhestandsalter als auch Alter beim Austritt zwischen 57 und 65 Jahre alt sind

tab job if fallnum != fallnum[_n+1]
		
count if fallnum != fallnum[_n+1]						// 
count


//*************************
// Personen im AltersRange
//*************************

*
* Hier M�ssen wir mal gemeinsam �berlegen!!!
*
* 
tab d_aa			
replace d_aa =  10 if d_aa >   9
replace d_aa =  -7 if d_aa <  -6						// wegen wenigen F�llen vor �bergang

tab d_rs
replace d_rs =  10 if d_rs >   9
replace d_rs =  -7 if d_rs <  -6						// wegen wenigen F�llen vor �bergang

count
drop if inlist(d_rs,10,-7)								//    91 Beobachtungen gel�scht (XX %)
count if fallnum != fallnum[_n+1]						// 
count

drop if inlist(d_aa,10,-7)								// 16039 Beobachtungen gel�scht (XX %)
count if fallnum != fallnum[_n+1]						// 
count

														// 16130 von 58,033 Beobachtungen (28%) gel�scht
sort fallnum
count if mi(aaj) & fallnum != fallnum[_n+1]


d, s													// 41903 Beobachtungen
count if fallnum != fallnum[_n+1]						//  3652 Personen	

drop if lz == .a
d, s													// 41799 Beobachtungen
count if fallnum != fallnum[_n+1]						//  3861 Personen	

bys art: tabstat lz, s(n mean) by(d_rs)

bys art: tabstat lz, s(n mean) by(d_aa)

bys job: tabstat lz, s(n mean) by(d_aa)
sort fallnum welle
tab job sex if fallnum != fallnum[_n+1], col
// Vorbereitung der Bildungsvariablen

if $an == 1 {	// Schalter oben!

preserve 
keep fallnum intjahr bild4 bildzeit4 isced4
collapse (firstnm) bild4 bildzeit4 isced4, by(fallnum)
rename bild4 bild
rename bildzeit4 bildzeit
rename isced4 isced
lab val bild pgsbil
lab val isced pgisced 
d, s											// 3784, aber 16 F�lle mit Missings! Beobachtungen
saveold "$path_temp\aa_soep_bild.dta", replace
restore
}

drop intjahr netto bildzeit4 isced4 bild4 intmon a_*check n1 aktu N* nsort n


for any 1 2 3: tab d_b art if fallnum != fallnum[_n+1] & job == X
tab d_b job if fallnum != fallnum[_n+1] , col
tab art job if fallnum != fallnum[_n+1] , col

d, s																	// 32193 (??) Beobachtungen
ds

sort fallnum welle
count if fallnum != fallnum[_n+1]										// f�r die 3861 Personen
saveold "$path_temp\aa_rs3.dta", replace

/* Deskriptionen

use "$path_temp\aa_rs3.dta", clear
tab job
drop if job == 2

tab rsj
preserve
keep if falln != falln[_n-1]

set scheme lean2
twoway histogram gebjahr, width(1) start(1910) bfcolor(none) blcolor(gs8) freq  ///
|| histogram rsj, width(1) start(1910) bfcolor(none) blcolor(gs2) barw(1) freq ///
		legend(order(1 "year of birth" 2 "year of retirement") pos(6) row(1)) xlabel(1910(10)2010)
graph export "$path_m/freq.png", replace

tabstat lz, by(d_rs) s(mean n)
bys art: tabstat lz, by(d_rs) s(mean n)

merge m:1 fallnum using "$path_temp\aa_soep_bild.dta"
gen bil3 = -1
replace bil3 = 1 if inlist(isced,0,1,2)
replace bil3 = 2 if inlist(isced,3,4)
replace bil3 = 3 if inlist(isced,5,6)
tab2 isced bil3, m
drop if bil3 == -1
tabstat lz if d_rs==0, by(bil3) s(mean n) 
bys art: tabstat lz if d_rs==0, by(bil3) s(mean n) 

for any -4 0 4 8: ginidesc lz if d_rs == X , by(art)
