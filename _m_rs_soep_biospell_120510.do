clear
*global path_do			"C:\Wissenschaft\04_RS_Monat"
global path_do			"H:\Diss\01_DA\03_Ruhestand\do"
do "$path_do\00_preamble_1.do"

**********************************************************************************************************************
di "DATUM LETZTE BEARBEITUNG DO-FILE: 11.05.12 "  $mydate                                                        	        
** ERSTER  BEARBEITER:               MWe                                                    			            ** 
** LETZTER BEARBEITER:               MWe                                       				                        **
**********************************************************************************************************************

**********************************************************************************************************************
** DO-FILE: 								                        	    	   				                    **
** BESCHREIBUNG (maximal ausf�hrlich: Was soll wie gemacht werden...			   									**		
**********************************************************************************************************************
/* 
	Datensatz BIOSPELL !

Erzeugen eines Datensatzes mit Ruhestandsdatumsinformationen (Monat und Jahr!)

Kriterien der Datenaufbereitung:
	1) Der Ruhestand wird die erste Angabe als �bergang in den Ruhestand gez�hlt.
	2) Bei LEWT wird die letzte Angabe als Ende der EWT gez�hlt.
	3) Darausfolgt: Es k�nnen unplausible F�lle entstehen, in denen der RS vor der LEWT liegt;
		--> diese wiedersprechen der Kriterien 1 und 2 und werden deshalb gel�scht.


*/
**********************************************************************************************************************
** 1: Ruhestandsinformation aus Artkalen generieren                                                  				**
**********************************************************************************************************************

do "$path_do/1_biospell.do"

**********************************************************************************************************************
** 2: Ursprungsdatens�tze nach Infos durchsuchen	 				                                                              				**
**********************************************************************************************************************

do "$path_do/2_rs_mon_soep_infos.do"

**********************************************************************************************************************
** 3: Ruhestandseintritte Voll&Teilzeit - Datenumstrukturierung	 				                                                              				**
**********************************************************************************************************************

do "$path_do/3_reshape.do"

**********************************************************************************************************************
** 4: Ruhestandseintritte Voll&Teilzeit - Erzeugung der Analysevariablen				                                                              				**
**********************************************************************************************************************

do "$path_do/4_prep_analysis.do"


**********************************************************************************************************************
** 5: EWT-Austritte - Datenumstrukturierung	 				                                                              				**
**********************************************************************************************************************

do "$path_do/5_reshape_aa.do"

**********************************************************************************************************************
** 6: EWT-Austritte - Erzeugung der Analysevariablen				                                                              				**
**********************************************************************************************************************

do "$path_do/6_prep_analysis_aa.do"


cd G:\MAs\Wetzel\Ruhestand\Artikel\data\Rente_alle
!mplus rente_alle_1.inp
!mplus rente_alle_1.inp    
!mplus rente_alle_2.inp   
!mplus rente_alle_3.inp   
!mplus rente_alle_4.inp      
!mplus rente_alle_5a.inp     
!mplus rente_alle_5b.inp  
!mplus rente_alle_5c.inp     
!mplus rente_alle_5d.inp     
!mplus rente_alle_6a.inp  
!mplus rente_alle_6b.inp     
!mplus rente_alle_6c.inp     
!mplus rente_alle_6d.inp  
!mplus rente_alle_6e.inp 

cd G:\MAs\Wetzel\Ruhestand\Artikel\data\Rente_direkt_VZ
!mplus rente_dir_1.inp
!mplus rente_dir_2.inp   
!mplus rente_dir_3.inp   
!mplus rente_dir_4.inp      
!mplus rente_dir_5a.inp     
!mplus rente_dir_5b.inp  
!mplus rente_dir_5c.inp     
!mplus rente_dir_5d.inp     
!mplus rente_dir_6a.inp  
!mplus rente_dir_6b.inp     
!mplus rente_dir_6c.inp     
!mplus rente_dir_6d.inp  
!mplus rente_dir_6e.inp  

cd G:\MAs\Wetzel\Ruhestand\Artikel\data\Rente_direkt_TZ
!mplus rente_dir_1.inp
!mplus rente_dir_2.inp   
!mplus rente_dir_3.inp   
!mplus rente_dir_4.inp      
!mplus rente_dir_5a.inp     
!mplus rente_dir_5b.inp  
!mplus rente_dir_5c.inp     
!mplus rente_dir_5d.inp     
!mplus rente_dir_6a.inp  
!mplus rente_dir_6b.inp     
!mplus rente_dir_6c.inp     
!mplus rente_dir_6d.inp  
!mplus rente_dir_6e.inp  

cd G:\MAs\Wetzel\Ruhestand\Artikel\data\Rente_direkt_alle
!mplus rente_dir_1.inp
!mplus rente_dir_2.inp   
!mplus rente_dir_3.inp   
!mplus rente_dir_4.inp      
!mplus rente_dir_5a.inp     
!mplus rente_dir_5b.inp  
!mplus rente_dir_5c.inp     
!mplus rente_dir_5d.inp     
!mplus rente_dir_6a.inp  
!mplus rente_dir_6b.inp     
!mplus rente_dir_6c.inp     
!mplus rente_dir_6d.inp  
!mplus rente_dir_6e.inp  

cd G:\MAs\Wetzel\Ruhestand\Artikel\data\Rente_ind
!mplus rente_indir_1.inp
!mplus rente_indir_2.inp   
!mplus rente_indir_3.inp   
!mplus rente_indir_4.inp      
!mplus rente_indir_5a.inp     
!mplus rente_indir_5b.inp  
!mplus rente_indir_5c.inp     
!mplus rente_indir_5d.inp     
!mplus rente_indir_6a.inp  
!mplus rente_indir_6b.inp     
!mplus rente_indir_6c.inp     
!mplus rente_indir_6d.inp  
!mplus rente_indir_6e.inp  

/*
cd G:\MAs\Wetzel\Ruhestand\Artikel\data\Austritt_ind
!mplus	austritt_indir_1.inp
!mplus	austritt_indir_2.inp
!mplus	austritt_indir_3.inp
!mplus	austritt_indir_4.inp
!mplus	austritt_indir_5a.inp
!mplus	austritt_indir_5b.inp
!mplus	austritt_indir_5c.inp
!mplus	austritt_indir_5d.inp
!mplus	austritt_indir_6a.inp
!mplus	austritt_indir_6b.inp
!mplus	austritt_indir_6c.inp
!mplus	austritt_indir_6d.inp
!mplus	austritt_indir_6e.inp
*/

