# Transition into Retirement Affects Life Satisfaction: Short- and Long-Term Development Depends on Last Labor Market Status and Education

This was joined work with Oliver Huxhold and Clemens Tesch-Römer. You find the article [here](http://link.springer.com/article/10.1007/s11205-015-0862-4). was published in *Social Indicators Research* in 2015.

For an overview of this project [see here](https://b-dx.gitlab.io/post1.html).

### Structure

Data preparation is conducted using stata 11. The _00_m_*.do_-File includes the central data preparation tasks.
The files _DCSM*.inp_ executes the final dual-change-score-models using MPlus.

## If you have questions,

please contact me [here](https://b-dx.gitlab.io/contact.html).
