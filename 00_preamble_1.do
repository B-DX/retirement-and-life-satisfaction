*clear matrix
set more off
capture log close
*set mem 350m
set mat 7000
set maxvar 9000

adopath + "H:\Projekte\stata_ado" 
adopath + "G:\Apps\Stata10\ado"

global mydate: di %tdCYND date(c(current_date), "DMY")

global an 0				// Synatxerweiterungen: 0 = aus, 1 = an
*net search runmplus

global path_in	   		"L:\DEAS\00_wellenuebergreifend\03_Daten\01_Panel"
global path_suf			"L:\DEAS\00_wellenuebergreifend\03_Daten\00_SUFs"
global path_temp 		"H:\Diss\01_DA\03_Ruhestand\temp"
global path_log  		"H:\Diss\01_DA\03_Ruhestand\log"
global path_out			"H:\Diss\01_DA\03_Ruhestand\dta"
global path_do			"H:\Diss\01_DA\03_Ruhestand\do"
global path_tex			"H:\Diss\01_DA\03_Ruhestand\tex"
global path_in_arb		"L:\DEAS\00_wellenuebergreifend\03_Daten\02_Arbeitsbelastung"
global path_m			"G:\MAs\Wetzel\Ruhestand\martin\soep"
global soep_in			"H:\SOEP\SOEPlong27\data"
global soep_in2			"H:\SOEP\SOEP27"

cap mkdir $path_temp
cap mkdir $path_log
cap mkdir $path_out
cap mkdir $path_tex
cap mkdir $path_m

log using "$path_log\_m_rs_soep_biospel_$mydate.smcl", replace
