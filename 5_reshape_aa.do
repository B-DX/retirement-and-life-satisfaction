*********************************
* Vollzeit�bergang in ArLoKe *
*********************************
use "$path_temp\aa_rs3.dta", clear

drop welle gr_ren* d_rs
drop spellinf //kalyear erhebj

// Variablen aus Liste l�schen
local a fallnum	bild a_rs sex gebjahr sample3 rsj_quel ///
	a_aa aam rsm rsj aaj loc1989 psample gebmonat pstyp d_bet d_bet_a d_bet2 art pszen ///
	pstyp rsbeg sort d_aa job
// Variablen zur Liste hinzuf�gen
qui ds _all
local b `r(varlist)'  

local help: list c | b
local help: list help - a 
di "`help'"
global help `help'

// der folgende Abbruch scheint ein bug in stata zu sein:
rename * *_
rename (fallnum_ d_aa_) (fallnum d_aa)


// Sortierung und Variablenst�mpfe
global levels n6 n5 n4 n3 n2 n1 0 1 2 3 4 5 6 7 8 9 
global order fallnum
global stumpf 

foreach var of global help {
	foreach suf of global levels {
		global order $order `var'_`suf'
		}
	global stumpf $stumpf `var'_
	}
di "$order"
*global stumpf $help
di "$stumpf"


****************************************************************************************
***! BINS-KONTROLLE
****************************************************************************************

* Um die Bins zu bilden, m�ssen die Distanzen zw. RS und Interviewzeitpunkt
* einmalig sein (sonst w�rde es zu einer �berschreibung einzelner Zellen kommen)
gen flag1 = d_aa == d_aa[_n+1] & fallnum == fallnum[_n+1]
tab flag1			
list fallnum d_aa aaj if flag1
bys fallnum flag1: gen drop = 1 if _n == 1 & flag1 == 1
								
* nur 4 Beobachtungen (0.0001 %)
drop if drop == 1
drop fla* drop

count if d_aa == .
*drop if d_rs == .

* negative Bins werden statt mit - (minus) mit n (negativ) bezeichnet
*	Grund: Variablennamen mit "-" in stata verboten
sort fallnum d_aa 
tostring d_aa, replace
foreach num of numlist 7 / 1 {
	replace d_aa = "n`num'" if d_aa == "-`num'"  
	}
tab d_aa



****************************************************************************************
***! RESHAPE und SORTIEREN
****************************************************************************************

reshape wide $stumpf, i(fallnum) j(d_aa) string

order $order

