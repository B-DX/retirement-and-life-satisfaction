// Daten aus ARTKALEN

local keepvars persnr spellnr spelltyp beginy endy spellinf zensor
use `keepvars' using "$soep_in2\pbiospe.dta", clear

// Ruhestandsspell
count if spelltyp == 8							// 12225 Ruhestandsspells (gesamt)

bys persnr spelltyp: gen N = _N if spelltyp == 8
tab N											// es gibt doppelte Ruhestandspells (pro Person)
*bro if N > 1
* Nur den ersten Ruhestand!
gen sort = .z
replace sort = 2 if spelltyp == 8				// Ruhestandsspells sind werden an Stelle 2 sortiert
replace sort = -2 if inlist(zensor,4,5,6,7,8,9) & sort == 2		// sort = -2 (nur nicht links zensierte Spells)

bys persnr spelltyp (beginy): gen flag = 1 if _n == 1 & sort == 2
* --> mehrere Ruhestands�berg�nge f�r manche Personen
tab N flag, m
replace sort = .v if flag != 1 & sort == 2 		// sort = -2 (nur erster RS-Spell)
drop flag 
count if sort == 2								// 11,119 erste, nicht linkszensierte Ruhestandsspells



// Bestimmen des letzten EWT-Spells
/* sowohl Teilzeit (spelltyp == 5) als auch Vollzeit (spelltyp == 4) */
gen d_bet 	= .z
gen rsbeg_h = beginy if sort == 2
bys persnr: egen rsbeg = total(rsbeg_h)
replace rsbeg = .y if rsbeg == 0
bro persnr beginy endy spelltyp sort zensor rsbe* d_bet

replace d_bet = rsbeg - endy if spelltyp != 8
* replace d_bet = .y if d_bet < 0
*	nur erste Angabe von RS !!!

* wieviele haben nach RS noch einmal VZ gearbeitet?
gen flag = 1 if spelltyp == 4 & d_bet < 0
bys persnr: egen VZiRS = total(flag)
tab VZiRS if sort == 2

* wieviele haben nach RS noch einmal TZ gearbeitet?
gen flag2 = 1 if spelltyp == 5 & d_bet < 0
bys persnr: egen TZiRS = total(flag2)
tab TZiRS if sort == 2

preserve
keep if VZiRS != 0 | TZiRS != 0
keep if sort == 2
keep persnr rsbeg beginy
keep if !mi(rsbeg) & inrange(rsbeg,1984,2009)
gen spelltyp = 10
saveold "$path_temp\stat1.dta", replace
restore

drop if VZiRS != 0
drop flag VZiRS						// 819 + 116 + 21 + 4 + 1 (961 // 1,89 %) gel�scht

drop if TZiRS != 0
drop flag2 TZiRS					// 814 + 73 + 12 (899 // 1,8 %) gel�scht	9259 Pers

preserve
keep if !mi(rsbeg) & inrange(rsbeg,1984,2009)
bys persnr: egen help = min(abs(d_bet)) if !inlist(zensor,2,3,5,6,8,9,.)
gen help2 = 1 if help == abs(d_bet) & !mi(d_bet) & !inlist(zensor,2,3,5,6,8,9,.)
tab spelltyp if help2 == 1
keep if help2 == 1
bys persnr (beginy): gen tag = _n == 1		// l�nger andauernder Spell
keep if tag == 1
duplicates report persnr 
keep persnr rsbeg spelltyp beginy
append using "$path_temp\stat1.dta"
saveold "$path_temp\stat1.dta", replace
restore

// Entscheidung f�r den LEWT-Spell, der die k�rzeste Distanz zum RS-Eintritt hat 
// und der nicht rechtszensiert ist
count if inlist(spelltyp,4,5)									// 89778 EWT-Spells (TZ und VZ)
count if inlist(spelltyp,4,5) & !inlist(zensor,2,3,5,6,8,9,.) 	// 57269 nicht-zensierte Spells
bys persnr: egen sort_h = min(abs(d_bet)) if inlist(spelltyp,4,5) & !inlist(zensor,2,3,5,6,8,9,.)
* links zensiert (KA): steht f�r fehlende Angaben auf linker Seite --> Entscheidung auch diese zu l�schen,
* da die pr�zise Abbildung des �bergangs die wichtige Grundlage der Arbeit sein wird

replace sort = 1 if sort_h == abs(d_bet) & !mi(d_bet) &  ///
		inlist(spelltyp,4,5) & !inlist(zensor,2,3,5,6,8,9,.)
bys persnr: egen nsort = total(sort==1) 
tab nsort									// aktuelle Stati beim � in RS haben Vorrang!
bro if inlist(nsort,2,3)
* Entscheidungshierarchie: Wenn mehrere Spells gleichzeitig vor RS enden
*1) Vollerwerbst�tigkeit
bys persnr: egen h_erw = total(sort ==1 & spelltyp==4)  
tab h_erw if nsort == 2 // alle haben eine VZ-EWT --> dies ist der entscheidende Spell
drop h_erw

bys persnr (sort spelltyp): gen n = _n
list 				if n == 1 & spelltyp != 4 & nsort == 2	// ok bei 0
replace sort = .v 	if n == 2 & spelltyp != 4 & nsort == 2	


// Job (Was ist der letzte Spelltyp)

bys persnr: egen sort_h2 = min(abs(d_bet)) if !inlist(zensor,2,3,5,6,8,9,.)

* Weiterhin wichtig: TeilzeitEWT m�glich trotz Ruhestand
gen aktu = .a if inrange(rsbeg,beginy,endy) & !inlist(zensor,2,3,5,6,8,9,.)
replace aktu = rsbeg - endy if aktu == .a & spelltyp != 8
replace aktu = .b if aktu == 0

tab aktu , m
/* replace sort = 1 if sort  != 1 & !mi(aktu) & 	// RSbeg in diesem VZ/TZ-Spell
bys persnr: egen nsort = total(sort==1) 
tab nsort									// aktuelle Stati beim � in RS haben Vorrang!





*/
bys persnr: egen N2 = total(sort==1)
tab N2

tab aktu spelltyp if sort == 1
gsort persnr sort beginy
replace sort = .x if persnr != persnr[_n-1] & sort[_n] == sort[_n+1] & sort == 1


sort persnr spellnr
gen 	job_h = spelltyp if 	aktu == d_bet & !mi(d_bet) 
replace job_h = spelltyp if  sort_h2 == d_bet & !mi(d_bet)
lab val job_h spelltyp

tab job_h
bys persnr: egen job_h_h = count(job_h)

* Entscheidungshierarchie: Wenn mehrere Spells angegeben, dann
*1) Vollerwerbst�tigkeit
bys persnr: egen h_erw = total(job_h==4)  
* 2) Teilzeit
bys persnr: egen h_tz  = total(job_h==5) 		if h_erw != 1 
* 3) Arbeitslos, Hausperson, Andere T�tigkeit (Mutterschaft)
bys persnr: egen h_arlo  = total(job_h == 6 )  	if h_erw != 1  &  h_tz != 1 
bys persnr: egen h_arlo2 = total(job_h == 7 )  	if h_erw != 1  &  h_tz != 1 & h_arlo != 1
bys persnr: egen h_arlo3 = total(job_h == 9 )  	if h_erw != 1  &  h_tz != 1 & h_arlo != 1 & h_arlo2 != 1

replace h_arlo = h_arlo2 if h_arlo == 0 & h_arlo2 == 1
replace h_arlo = h_arlo3 if h_arlo == 0 & h_arlo2 == 0 & h_arlo3 == 1

*bys persnr job_h: replace job_c = job_h if job_c != 999 & _n == 1
* replace job_ch2 = .y if job_c == .y

gen job = -1
* bys persnr: egen job = total(job_c)

for any erw tz arlo arlo2 arlo3 \ any 1 2 3 3 3 : replace job = Y if h_X == 1
* replace job = -2 if job_h_h == 1 & job < 0
* tab job if persnr != persnr[_n+1] , m
count if d_bet != . & job == 0	//ok

lab def job 1 "Vollerw." 2 "Teilzeit" 3 "Hausperson/ArloKe" -2 "fehlende Zuordnung" ///
			-1 "kein vorhergehender Spell vorhanden"
lab val job job
tab job if persnr != persnr[_n+1] , m
tab job if persnr != persnr[_n+1] , m
tab d_bet job if sort == 1, m

drop rsbeg rsbeg_h sort_h N sort_h2
drop h_* job_*

bys persnr: egen N3 = count(sort ) if sort == 1
tab N3
tab sort


tab aktu spelltyp if sort == 1

count if sort == 1						// 10,263 LEWT, nicht rechtszentrierte Spells
tab sort, m


drop if !inlist(sort,1,2)
tab sort								// 5,053 RS; 5,053 Austritte --> keine Typbildung f�r 582  F�lle

bys persnr: gen N = _N
tab N sort											// * 6 Personen mit VZ und TZ-LEWT: VZ schl�gt TZ
tab spelltyp if N > 2 & sort == 1
drop if N == 3 & sort == 1 & spelltyp == 5
bys persnr: egen flag = total(N == 1)
sort persnr sort
drop N

// Pre-Spells 	in aktuelle Zeile schreiben	(ps..)
bys persnr (sort): gen pstyp  = spelltyp[_n-1] 	if spelltyp == 8	// nur inlist(4)
bys persnr (sort): gen psbeg  = beginy[_n-1]    if spelltyp == 8
bys persnr (sort): gen psend  = endy[_n-1]    	if spelltyp == 8
bys persnr (sort): gen pszen  = zensor[_n-1]	if spelltyp == 8
bys persnr (sort): replace d_bet  = d_bet[_n-1]	if spelltyp == 8
bys persnr (sort): replace aktu   =  aktu[_n-1]	if spelltyp == 8
lab val pstyp spelltyp
lab val pszen zensor
lab val psbeg begin
lab val psend end 


bys persnr: gen N = _N
tab N sort											// 530 Personen ohne LEWT-Spell

* bro if flag != 0

drop if N 	== 1									// kein rechtszentrierter vorhanden LEWT-Spell

drop if sort == 1									// Alle Infos in einer Zeile
tab pstyp, m										// keine  Missings! 	8729 Personen
tab job pstyp

rename beginy 	rsj			// Begin des Ruhestandsspells
rename psend 	aaj			// Ende des LEWT-Spells

list persnr rsj aaj in 20/50, noobs clean

drop spellnr zensor N flag spelltyp psbeg pszen 

d, s

saveold "$path_temp\biospell_1.dta", replace
