

merge 1:1 fallnum using "$path_temp\aa_soep_bild.dta"
keep if _m == 3
drop _m

replace sex = sex - 1

numlabel pgsbil pgisced, add
/*
Stufe 1 � niedrige Bildung: ISCED 0-2; 
bezogen auf DEAS: ZPs ohne abgeschlossene berufliche Ausbildung (ausgenommen ZPs mit h�herem allgemeinbildenden Schulabschluss - auch "weiterf�hrende Schule" bei x28, falls mehr als 10 Jahre Schulbesuch bei x27a)

Stufe 2 � mittlere Bildung: ISCED 3-4; 
bezogen auf DEAS: ZPs mit Abschluss einer betrieblichen oder berufsbildend-schulischen Ausbildung, einschlie�lich ZPs mit h�herem allgemeinbildenden Schulabschluss ohne abgeschlossene Berufsausbildung - auch "weiterf�hrende Schule" bei x28, falls mehr als 10 Jahre Schulbesuch bei x27a

Stufe 3 � hohe Bildung: ISCED 5-6; 
bezogen auf DEAS: ZPs mit abgeschlossener Aufstiegsfortbildung (Fach-, Meister-, Technikerschule, Berufs- oder Fachakademie) und ZPs mit abgeschlossenem Studium (FHS, Hochschule)
*/
gen bil3 = -1
replace bil3 = 1 if inlist(isced,0,1,2)
replace bil3 = 2 if inlist(isced,3,4)
replace bil3 = 3 if inlist(isced,5,6)
tab2 isced bil3, m
drop if bil3 == -1

egen h_count = rownonmiss(lz_n6 - lz_8)
tab h_count
sum h_count	if inlist(job,1,3)		// durchschnittl. 8,11 Beobachtungen pro Person (SD +- 4,33)

bys job: sum d_bet if inlist(job,1,3)

tab gebjahr
gen kohort = -1
replace kohort = 1 if gebjahr < 1930
replace kohort = 2 if gebjahr >= 1930 & gebjahr < 1940
replace kohort = 3 if gebjahr >= 1940
tab kohort, m
rename gebjahr gjahr
rename gebmonat gmon
rename famstd* fstd*

egen sG 	= rowlast(sG_*)
egen fG 	= rowlast(fG_*)
egen hhek 	= rowlast(hhek_*)
egen fstd 	= rowlast(fstd_*)
drop sG_* fG_* hhek_* fstd_*

tab2 d_bet_ art_ 

tabstat lz_*, by(art) s(n)

gen d_aj = rsj - aaj 			// Distanz zu letzter EWT
tab d_aj job, m

* ++++++++++++++++++
* T-Standardisierung
* ++++++++++++++++++
global tstd = 0
if $tstd == 1 {
qui sum lz_0
local sd `r(sd)'
local mean `r(mean)'

for any $levels: replace lz_X = ((lz_X - `mean') / `sd' ) * 10 + 50
tabstat lz_n6 - lz_9, s(mean v n) 
}

//				2 Datens�tze: der erste mit allen Informationen, aber nicht absoluter Sicherheit �ber RSJ
preserve
tabstat lz_n6 - lz_8, s(mean v n) 
drop rsj* bild isced loc19* psampl* psty* aa* endy*
rename *_ *

saveold "$path_out\soep_rsj.dta", replace

stata2mplus using "$path_m\rs_soep_$mydate", replace

// Gruppenstatistik 
tabstat sex a_rs, by(job) s(mean n) save
mat A1 = r(Stat1)
mat A2 = r(Stat2)
mat A3 = r(Stat3)
tabstat sex a_rs, by(bil3) s(mean n) save
mat B1 = r(Stat1)
mat B2 = r(Stat2)
mat B3 = r(Stat3)
collapse (count) fallnum (mean) sex a_rs      , by(job bil3 )
drop if job == .a
set obs 15
for any 10 11 12 \ any 1 2 3: replace job = Y in X
for any 10 11 12: replace bil3 = 4 in X
for any 13 14 15 \ any 1 2 3: replace bil = Y in X
for any 13 14 15: replace job = 4 in X
for any 1 2 3: replace sex = AX[1,1] 		if job == X & bil3 == 4
for any 1 2 3: replace a_rs = AX[1,2] 		if job == X & bil3 == 4
for any 1 2 3: replace fallnum = AX[2,1] 	if job == X & bil3 == 4
for any 1 2 3: replace sex = BX[1,1] 		if job == 4 & bil3 == X
for any 1 2 3: replace a_rs = BX[1,2] 		if job == 4 & bil3 == X
for any 1 2 3: replace fallnum = BX[2,1] 	if job == 4 & bil3 == X
sort job bil3
list
outputst "$path_out\tabstat2.xls" /y
restore


exit

// Testauswertung - Mittelwerte sch�tzen lassen

local a fallnum
local b `r(varlist)'
local v: list b - a

qui ds 

runmplus  lz_n5 - lz_10                                                           ///
         , model(                                                       ///
      [lz_n5 lz_n4 lz_n3 lz_n2 lz_n1 lz_0 lz_1 lz_2 lz_3];                                            ///     
      [lz_4 lz_5 lz_6 lz_7 lz_8 lz_9 lz_9];                     		///
          )                                                             ///
          savelogfile("$path_m\2_log_m+_lz")

