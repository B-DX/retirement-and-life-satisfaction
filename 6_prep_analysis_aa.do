

merge 1:1 fallnum using "$path_temp\aa_soep_bild.dta"
keep if _m == 3
drop _m

replace sex = sex - 1

numlabel pgsbil pgisced, add
/*
Stufe 1 � niedrige Bildung: ISCED 0-2; 
bezogen auf DEAS: ZPs ohne abgeschlossene berufliche Ausbildung (ausgenommen ZPs mit h�herem allgemeinbildenden Schulabschluss - auch "weiterf�hrende Schule" bei x28, falls mehr als 10 Jahre Schulbesuch bei x27a)

Stufe 2 � mittlere Bildung: ISCED 3-4; 
bezogen auf DEAS: ZPs mit Abschluss einer betrieblichen oder berufsbildend-schulischen Ausbildung, einschlie�lich ZPs mit h�herem allgemeinbildenden Schulabschluss ohne abgeschlossene Berufsausbildung - auch "weiterf�hrende Schule" bei x28, falls mehr als 10 Jahre Schulbesuch bei x27a

Stufe 3 � hohe Bildung: ISCED 5-6; 
bezogen auf DEAS: ZPs mit abgeschlossener Aufstiegsfortbildung (Fach-, Meister-, Technikerschule, Berufs- oder Fachakademie) und ZPs mit abgeschlossenem Studium (FHS, Hochschule)
*/
gen bil3 = -1
replace bil3 = 1 if inlist(isced,0,1,2)
replace bil3 = 2 if inlist(isced,3,4)
replace bil3 = 3 if inlist(isced,5,6)
tab bil3, m
drop if bil3 == -1


egen h_count = rownonmiss(lz*)
tab h_count


tab gebjahr
gen kohort = -1
replace kohort = 1 if gebjahr < 1930
replace kohort = 2 if gebjahr >= 1930 & gebjahr < 1940
replace kohort = 3 if gebjahr >= 1940
tab kohort
rename gebjahr gjahr

tab2 d_bet art_ 

*replace art = 1 if d_bet == 0
*replace art = 2 if inrange(d_bet,1,7)
*tab art 
tabstat lz_*, by(art) s(n)

* ++++++++++++++++++
* T-Standardisierung
* ++++++++++++++++++
global tstd = 0
if $tstd == 1 {
qui sum lz_0
local sd `r(sd)'
local mean `r(mean)'

for any $levels: replace lz_X = ((lz_X - `mean') / `sd' ) * 10 + 50
tabstat lz_n6 - lz_9, s(mean v n) 
}

//				2 Datens�tze: der erste mit allen Informationen, aber nicht absoluter Sicherheit �ber RSJ
preserve

bys art: tabstat lz_n6 - lz_9, s(mean v n) 
sort fallnum
drop rsj* bild isced loc19* psampl* psty* aa* endy*
rename *_ *
ds 

saveold "$path_out\soep_rsj_aa.dta", replace

stata2mplus using "$path_m\rs_soep_aa_$mydate", replace
restore

exit

// Testauswertung - Mittelwerte sch�tzen lassen

local a fallnum
local b `r(varlist)'
local v: list b - a

qui ds 

runmplus  lz_n5 - lz_10                                                           ///
         , model(                                                       ///
      [lz_n5 lz_n4 lz_n3 lz_n2 lz_n1 lz_0 lz_1 lz_2 lz_3];                                            ///     
      [lz_4 lz_5 lz_6 lz_7 lz_8 lz_9 lz_10];                     		///
          )                                                             ///
          savelogfile("$path_m\2_log_m+_lz")

